import './App.scss';

import HomePage from './components/home/Home';

function App() {
  return (
    <div className="App">
      <HomePage />
    </div>
  );
}

export default App;
