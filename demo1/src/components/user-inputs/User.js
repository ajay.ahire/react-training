import '../user-inputs/User.scss';

const User = (props) => {

    const user = {
        name: 'Ajay Ahire',
    };

    const onClick = (event) => {
        props.sendData(user);
    }

    return (
        <div className='userName-input'>
            <label className='label'>UserName:</label>
            <input type="text" name='username'></input>
            <button onClick={onClick}>submit</button>
        </div>
    );
};

export default User;