import '../header-section/Header.scss';

import React from "react";

const HeaderBar = (props) => {
    console.log(props);
    return (
        <div className='header-title'>
            <h1>Hello {props.name}</h1>
            <p>{props.children}</p>
        </div>
    )
}

export default HeaderBar;