import { Menu, MenuItem, Sidebar, SubMenu } from 'react-pro-sidebar';

import React from "react";

const SideBar = () => {
    return (
        <Sidebar className='side-bar'>
            <Menu>
                <SubMenu label="Charts" className='sub-menu'>
                    <MenuItem> Pie charts </MenuItem>
                    <MenuItem> Line charts </MenuItem>
                </SubMenu>
                <SubMenu label="Maps">
                    <MenuItem> Google Maps </MenuItem>
                    <MenuItem> Open Street Maps </MenuItem>
                </SubMenu>
                <MenuItem> Documentation </MenuItem>
                <MenuItem> Calendar </MenuItem>
            </Menu>
        </Sidebar>
    )
}

export default SideBar;