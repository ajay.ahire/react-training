import '../home/Home.scss';
import '../header-section/Header.scss'

import React, { useState } from "react";

import HeaderBar from '../header-section/Header';
import SideBar from '../side-bar/Sidebar';
import User from '../user-inputs/User';

const HomePage = () => {

    const [data, setData] = useState({
        name: '?'
    });

    const sendData = (data) => {
        setData(data);
    }

    return (
        <>
            <HeaderBar name='Clark'>
                <p>This is new header</p>
            </HeaderBar>
            <HeaderBar name='Diana'>
                <p>This is new header 1</p>
            </HeaderBar>
            <div className='home-body-content'>
                <div className='side-bar-content'><SideBar /></div>
                <div className='user-content'><User sendData={sendData} /></div>
            </div>

            <div>
                The user data sent from Child compoent:
                <strong>{data.name}</strong>
            </div>

        </>
    )
}

export default HomePage;